# grub

TODO: `grub-mkconfig -o /boot/grub/grub.cfg`

## efi setup

See [/etc/grub.d/40_efi_setup](40_efi_setup)

## multiple entries

* disable submenu
* remember last selection

Archwiki: [GRUB/Tips_and_tricks#Multiple_entries](https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Multiple_entries)
