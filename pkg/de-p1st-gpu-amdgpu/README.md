# AMD Graphics Card

Good overview (table):

* [wiki.gentoo.org/Radeon](https://wiki.gentoo.org/wiki/Radeon#Feature_support)
* [wiki.gentoo.org/AMDGPU](https://wiki.gentoo.org/wiki/AMDGPU#Feature_support)

## AMDGPU

From gentoo wiki one should use AMDGPU e.g. for:

> AMD Zen 2 "4000" series 7nm laptop APUs

### AMDGPU (mandatory packages)

* [https://wiki.archlinux.org/title/Xorg#AMD]()
  * which driver for which GPU architecture
* [https://wiki.archlinux.org/title/Lenovo_IdeaPad_S540_13ARE]()
  * Laptop with Ryzen 7 4800U CPU


* `amdgpu` module
  * [https://wiki.archlinux.org/title/AMDGPU#Loading]()
    * The amdgpu kernel module is supposed to load automatically
      on system boot.
    * It is possible it loads, but late, after the X server requires it.
      In this case:
      * Include "amdgpu" in MODULES=(...) of `/etc/mkinitcpio.conf`
  * [https://wiki.archlinux.org/title/AMDGPU#Specify_the_correct_module_order]()
    * `amdgpu` before `radeon`

---

* [https://wiki.archlinux.org/title/AMDGPU]()
* [https://wiki.archlinux.org/title/Hardware_video_acceleration#ATI/AMD]()
  * VA-API, VDPAU, AMF

```shell
# DRI driver for 3D acceleration.
depends+=(mesa)
optdepends+=('lib32-mesa: 32bit app support')
# DDX driver (which provides 2D acceleration in Xorg).
depends+=(xf86-video-amdgpu)
# Vulkan support.
depends+=(vulkan-radeon)
optdepends+=('lib32-vulkan-radeon: 32bit app support')
# HW-vide-acceleration: VA-API.
depends+=(libva-mesa-driver)
optdepends+=('lib32-libva-mesa-driver: 32bit app support')
# HW-vide-acceleration: VDPAU.
depends+=(mesa-vdpau)
optdepends+=('lib32-mesa-vdpau: 32bit app support')
```

### AMDGPU Verification

```shell
lspci -k | grep -EA3 'VGA|3D|Display'
```

Example output:

```
Kernel driver in use: amdgpu
Kernel modules: amdgpu
```

### HW-video-acceleration verification

```shell
sudo pacman -S --needed libva-utils vdpauinfo
```

```shell
vainfo
```
```
vainfo: VA-API version: 1.14 (libva 2.14.0)
vainfo: Driver version: Mesa Gallium driver 22.0.3 for AMD RENOIR (LLVM 13.0.1, DRM 3.42, 5.15.38-1-lts)
vainfo: Supported profile and entrypoints
      VAProfileMPEG2Simple            :	VAEntrypointVLD
      VAProfileMPEG2Main              :	VAEntrypointVLD
      VAProfileVC1Simple              :	VAEntrypointVLD
      VAProfileVC1Main                :	VAEntrypointVLD
      VAProfileVC1Advanced            :	VAEntrypointVLD
      VAProfileH264ConstrainedBaseline:	VAEntrypointVLD
      VAProfileH264ConstrainedBaseline:	VAEntrypointEncSlice
      VAProfileH264Main               :	VAEntrypointVLD
      VAProfileH264Main               :	VAEntrypointEncSlice
      VAProfileH264High               :	VAEntrypointVLD
      VAProfileH264High               :	VAEntrypointEncSlice
      VAProfileHEVCMain               :	VAEntrypointVLD
      VAProfileHEVCMain               :	VAEntrypointEncSlice
      VAProfileHEVCMain10             :	VAEntrypointVLD
      VAProfileHEVCMain10             :	VAEntrypointEncSlice
      VAProfileJPEGBaseline           :	VAEntrypointVLD
      VAProfileVP9Profile0            :	VAEntrypointVLD
      VAProfileVP9Profile2            :	VAEntrypointVLD
      VAProfileNone                   :	VAEntrypointVideoProc
```

```shell
vdpauinfo
```
```
yoda@yodaTux ~ % vdpauinfo
display: :0.0   screen: 0
API version: 1
Information string: G3DVL VDPAU Driver Shared Library version 1.0

Video surface:

name   width height types
-------------------------------------------
420    16384 16384  NV12 YV12 
422    16384 16384  UYVY YUYV 
444    16384 16384  Y8U8V8A8 V8U8Y8A8 
420_16 16384 16384  
422_16 16384 16384  
444_16 16384 16384  

Decoder capabilities:

name                        level macbs width height
----------------------------------------------------
MPEG1                          --- not supported ---
MPEG2_SIMPLE                    3 78336  4096  4906
MPEG2_MAIN                      3 78336  4096  4906
H264_BASELINE                  52 78336  4096  4906
H264_MAIN                      52 78336  4096  4906
H264_HIGH                      52 78336  4096  4906
VC1_SIMPLE                      1 78336  4096  4906
VC1_MAIN                        2 78336  4096  4906
VC1_ADVANCED                    4 78336  4096  4906
MPEG4_PART2_SP                  3 78336  4096  4906
MPEG4_PART2_ASP                 5 78336  4096  4906
DIVX4_QMOBILE                  --- not supported ---
DIVX4_MOBILE                   --- not supported ---
DIVX4_HOME_THEATER             --- not supported ---
DIVX4_HD_1080P                 --- not supported ---
DIVX5_QMOBILE                  --- not supported ---
DIVX5_MOBILE                   --- not supported ---
DIVX5_HOME_THEATER             --- not supported ---
DIVX5_HD_1080P                 --- not supported ---
H264_CONSTRAINED_BASELINE       0 78336  4096  4906
H264_EXTENDED                  --- not supported ---
H264_PROGRESSIVE_HIGH          --- not supported ---
H264_CONSTRAINED_HIGH          --- not supported ---
H264_HIGH_444_PREDICTIVE       --- not supported ---
VP9_PROFILE_0                  --- not supported ---
VP9_PROFILE_1                  --- not supported ---
VP9_PROFILE_2                  --- not supported ---
VP9_PROFILE_3                  --- not supported ---
HEVC_MAIN                      186 139264  8192  4352
HEVC_MAIN_10                   186 139264  8192  4352
HEVC_MAIN_STILL                --- not supported ---
HEVC_MAIN_12                   --- not supported ---
HEVC_MAIN_444                  --- not supported ---
HEVC_MAIN_444_10               --- not supported ---
HEVC_MAIN_444_12               --- not supported ---

Output surface:

name              width height nat types
----------------------------------------------------
B8G8R8A8         16384 16384    y  NV12 YV12 UYVY YUYV Y8U8V8A8 V8U8Y8A8 P010 P016 A8I8 I8A8 
R8G8B8A8         16384 16384    y  NV12 YV12 UYVY YUYV Y8U8V8A8 V8U8Y8A8 P010 P016 A8I8 I8A8 
R10G10B10A2      16384 16384    y  NV12 YV12 UYVY YUYV Y8U8V8A8 V8U8Y8A8 P010 P016 A8I8 I8A8 
B10G10R10A2      16384 16384    y  NV12 YV12 UYVY YUYV Y8U8V8A8 V8U8Y8A8 P010 P016 A8I8 I8A8 

Bitmap surface:

name              width height
------------------------------
B8G8R8A8         16384 16384
R8G8B8A8         16384 16384
R10G10B10A2      16384 16384
B10G10R10A2      16384 16384
A8               16384 16384

Video mixer:

feature name                    sup
------------------------------------
DEINTERLACE_TEMPORAL             y
DEINTERLACE_TEMPORAL_SPATIAL     -
INVERSE_TELECINE                 -
NOISE_REDUCTION                  y
SHARPNESS                        y
LUMA_KEY                         y
HIGH QUALITY SCALING - L1        y
HIGH QUALITY SCALING - L2        -
HIGH QUALITY SCALING - L3        -
HIGH QUALITY SCALING - L4        -
HIGH QUALITY SCALING - L5        -
HIGH QUALITY SCALING - L6        -
HIGH QUALITY SCALING - L7        -
HIGH QUALITY SCALING - L8        -
HIGH QUALITY SCALING - L9        -

parameter name                  sup      min      max
-----------------------------------------------------
VIDEO_SURFACE_WIDTH              y        48     4096
VIDEO_SURFACE_HEIGHT             y        48     4096
CHROMA_TYPE                      y  
LAYERS                           y         0        4

attribute name                  sup      min      max
-----------------------------------------------------
BACKGROUND_COLOR                 y  
CSC_MATRIX                       y  
NOISE_REDUCTION_LEVEL            y      0.00     1.00
SHARPNESS_LEVEL                  y     -1.00     1.00
LUMA_KEY_MIN_LUMA                y  
LUMA_KEY_MAX_LUMA                y  
```

### AMDGPU PRO (optional proprietary packages)

* [https://wiki.archlinux.org/title/AMDGPU_PRO]()

The `amdgpu-pro-installer` contains proprietary components for
AMDGPU (it works on top of AMDGPU). (AUR: Search by `Package Base`, Keywords `amdgpu-pro-installer`)

---

* [https://wiki.archlinux.org/title/AMDGPU_PRO#Installation]()

```shell
# Proprietary OpenGL implementation.
depends+=(amdgpu-pro-libgl)
optdepends+=('lib32-amdgpu-pro-libgl: 32bit app support')
# Proprietary OpenCL implementation.
# Alternatively: `opencl-mesa`.
depends+=(opencl-amd)
# Proprietary Vulkan implementation.
depends+=(vulkan-amdgpu-pro)
optdepends+=('lib32-vulkan-amdgpu-pro: 32bit app support')
# HW-video-acceleration:
# - AMDGPU PRO proprietary driver is built on top of AMDGPU driver and supports both VA-API and VDPAU in addition to AMF.
#
# Advanced Media Framework implementation.
depends+=(amf-amdgpu-pro)
```
