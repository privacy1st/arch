# systemd configuration

* time synchronization
  * Archwiki: [systemd-timesyncd](https://wiki.archlinux.org/index.php/systemd-timesyncd)
  * Archwiki: [System Time -> Time synchronization -> systemd-timesyncd](https://wiki.archlinux.org/title/System_time#Time_synchronization)
    
* journal size limit
  * Archwiki: [Systemd/Journal#Journal_size_limit](https://wiki.archlinux.org/index.php/Systemd/Journal#Journal_size_limit)
