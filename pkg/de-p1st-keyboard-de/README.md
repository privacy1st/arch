# Virtual Console keyboard mapping

==> VC keymap

`localectl status`
* list current console (VC) and xorg (X11) keymap

`localectl list-keymaps | grep de`
* List available console keyboard mappings and filter German ones, e.g. "de-latin1-nodeadkeys"

`/etc/vconsole.conf`
* persistent console keymap

Archwiki: [Installation_guide#Localization](https://wiki.archlinux.org/index.php/Installation_guide#Localization)
