# fonts

For font configuration, see: [archwiki#font-config](https://wiki.archlinux.org/index.php/Font_configuration)


## types of fonts

Outline formats

* TrueType by Apple and Microsoft (file extension: `ttf`)
* OpenType by Microsoft, built on TrueType (file extensions: `otf`, `ttf`)

## installed fonts

[https://wiki.archlinux.org/index.php/Fonts#Tips_and_tricks](https://wiki.archlinux.org/index.php/Fonts#Tips_and_tricks)

List all fonts:

```shell
fc-list
```

To search for monospace fonts supporting a particular Unicode codepoint:

```shell
fc-match -s monospace:charset=1F4A9
```

## Font packages

[archwiki](https://wiki.archlinux.org/index.php/Fonts#Font_packages)

Install fonts with _Unicode_ keyword.

## installation


```shell
sudo pacman -S --needed noto-fonts
```

```shell
aur sync -c ttf-google-fonts-git
```

```shell
aur sync -c ttf-ms-fonts
```

* [https://typeof.net/Iosevka/]()

```shell
aur sync -c ttf-iosevka
```

```shell
aur sync -c ttf-iosevka-term
```

## emoji font

- [https://wiki.archlinux.org/index.php/Fonts#Emoji_and_symbols]()

```shell
sudo pacman -S --needed noto-fonts-emoji
```
