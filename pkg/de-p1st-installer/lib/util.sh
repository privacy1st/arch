function join_by() {
  # Join array elements with character $1
  #
  # arg $1: delimiter
  # arg $2: name of source array
  # arg $3: variable name to store result

  local -n ptr=$2  || return $?
  local -n ptr2=$3 || return $?
  ptr2=$(printf ',%s' "${ptr[@]}")
  ptr2=${ptr2:1}
}

function newline_to_space() {
  # Replaces all newlines with spaces
  #
  # arg $1: name of variable

  local -n ptr=$1 || return $?
  # Replace newlines with spaces
  # See bash string substitution: https://gist.github.com/JPvRiel/b279524d3e56229a896477cb8082a72b

  # echo "replacing newlines in str: -->$ptr<--"
  ptr="${ptr//$'\n'/' '}"
  # echo "new str is: -->$ptr<--"
}

function newline_separated_to_array() {
  # Watch out for tailing newlines as these will get an empty array entry at the end.
  # $1 and $2 can be equal (if the result shall be written to the input variable)
  #
  # arg $1: name of variable with newline separated list
  # arg $2: name of array to store values into

  local -n ptr=$1  || return $?
  local -n ptr2=$2 || return $?
  # ptr newline separated list.
  # Store this as array ptr2.
  readarray -t ptr2 <<<"${ptr}"
}

function space_separated_to_array() {
  # arg $1: name of variable with space separated list
  # arg $2: name of array to store values into

  local -n ptr=$1  || return $?
  # shellcheck disable=SC2178
  local -n ptr2=$2 || return $?
  # ptr space separated list.
  # Store this as array ptr2.
  # Without newline at last array element: https://unix.stackexchange.com/a/519917/315162
  readarray -d ' ' -t ptr2 < <(printf '%s' "${ptr}")
}

function get_cpu_vendor() {
  # @pre
  #   CPU_VENDOR ("", "autodetect")
  # @post
  #   CPU_VENDOR ("amd", "intel", "none", "")

  if [[ -z "${CPU_VENDOR}" ]] || [[ "${CPU_VENDOR}" == 'autodetect' ]]; then

    # If run virtual environment (e.g. VirtualBox) then no CPU microcode is required
    if cat /proc/cpuinfo | grep '^flags.*hypervisor' >/dev/null; then
      echo 'Detected virtual environment.'
      CPU_VENDOR='none'

    else
      local vendor_id
      vendor_id=$(cat /proc/cpuinfo | grep vendor_id | head -1 | sed 's|vendor_id\s*:\s*||')

      if [ "${vendor_id}" = 'AuthenticAMD' ]; then
        CPU_VENDOR='amd'
      elif [ "${vendor_id}" = 'GenuineIntel' ]; then
        CPU_VENDOR='intel'
      else
        echo 'Unknown CPU vendor'
        return 1
      fi
    fi

  fi
}
