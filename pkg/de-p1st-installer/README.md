# Arch Installer

## Running the installer

### Via ssh and custom livemedium

1) Build custom [archiso](https://gitlab.archlinux.org/archlinux/archiso) live medium using docker in privileged mode:

```shell
# dependencies: docker, docker-compose

cd build-iso
# sudo systemctl start docker
sudo docker-compose run archiso
ls out/out_dir/*.iso
```

**On the target computer:**

2) Boot into the live medium
3) Connect to the Internet
  * [wifi instructions](https://wiki.archlinux.org/title/Iwd#iwctl)

**On some other computer:**

4) Connect to target computer via `ssh`
  * See [build-iso/out/out_dir/ssh-host-fingerprints](../../build-iso/out/out_dir/ssh-host-fingerprints)
    for verification of the ssh fingerprint of the target computer
5) (Optional) Copy and adjust one of the example configurations (`/etc/de-p1st-installer/*.cfg`) to `/etc/de-p1st-installer/installer.cfg`
   * If this step is skipped, then (most) of the configuration can be entered interactively
   * TODO: if `ADDITIONAL_PKGS` is empty, interactively ask for it.
6) Run the installer:

```shell
de-p1st-installer
```

### Via official livemedium

1) Boot into [official live medium](https://archlinux.org/download/)
2) Add to `/etc/pacman.conf`:

```shell
[de-p1st]
SigLevel = Optional TrustAll
Server = https://arch.p1st.de
```

3) (Optional) Install screen with a `screenrc` configuration file for long scrollback history
   
```shell
pacman -Sy
pacman -S de-p1st-screen

# And then start it with:
screen
```

4) Install `de-p1st-installer` to get the install script
   
```shell
pacman -Sy
pacman -S de-p1st-installer
```

5) Continue with step (5) of "Via custom livemedium"
