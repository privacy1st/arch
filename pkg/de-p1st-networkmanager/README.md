## vpn-support

* [https://wiki.archlinux.org/index.php/NetworkManager#VPN_support]()

For example the **openvpn** plugin:

```
sudo pacman -S --needed openvpn networkmanager-openvpn
```

Or L2P and **IPsec**:

```
aur sync -c libreswan
aur sync -c networkmanager-libreswan
aur sync -c networkmanager-l2tp
```

See tutorail here for details on how to connect with IPsec:

* https://www.tecmint.com/setup-l2tp-ipsec-vpn-client-in-linux/

## local network hostname resolution

* [https://wiki.archlinux.org/index.php/Network_Configuration#Local_network_hostname_resolution]()

Either with `systemd-resolved` or `Avahi`.
As we use `systemd-resolved` Avahi is not required.

* `pacman -S avahi` and
  `systemctl enable enable avahi-daemon.service`
  
