# Adwaita theme - in GTK and QT

* [https://wiki.archlinux.org/index.php/Uniform_look_for_Qt_and_GTK_applications#Adwaita]()

**NOTE**: This has just been tested with XFCE4 so far!

## GTK

If using XFCE4 or Gnome this is the default theme. No action required.

## qt5

```shell
sudo pacman -S --needed adwaita-qt5
```

Enable via env variable.

```shell
echo 'export QT_STYLE_OVERRIDE=adwaita' | sudo tee -a /etc/profile.d/99_qt5_theme.sh
```

## qt4

```shell
aur sync -c adwaita-qt4
```

Then edit `/etc/xdg/Trolltech.conf` :

```
# ...
[Qt]
style=adwaita
# ...
```

## A quote that gave me this idea

> If you are using a GTK-based environment you need to configure
> Qt applications look and feel (using 
> [adwaita-qt](https://github.com/FedoraQt/adwaita-qt) `qt5ct` 
> `qt5-styleplugins`. You can read
> [this wiki page](https://wiki.archlinux.org/index.php/Uniform_look_for_Qt_and_GTK_applications#Styles_for_both_Qt_and_GTK)
> about it.
> 