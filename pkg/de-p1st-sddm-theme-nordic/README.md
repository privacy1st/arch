# SDDM themes

* https://www.opendesktop.org/browse/cat/101/page/2/ord/rating/
* https://aur.archlinux.org/packages/?O=0&SeB=nd&K=sddm+theme&outdated=&SB=p&SO=d&PP=50&do_Search=Go

## Nordic

* https://github.com/EliverLara/Nordic

Install from AUR:

```shell
sudo pacman -S sddm-nordic-theme-git
```

Test the theme:

```shell
sddm-greeter --test-mode --theme /usr/share/sddm/themes/Nordic
```

## Other themes

```shell
sudo pacman -S chili-sddm-theme
sddm-greeter --test-mode --theme /usr/share/sddm/themes/chili
```

```shell
sudo pacman -S sddm-theme-deepin-git
sddm-greeter --test-mode --theme /usr/share/sddm/themes/deepin/
```

```shell
sudo pacman -S --needed plasma-workspace
sddm-greeter --test-mode --theme /usr/share/sddm/themes/breeze/
```
