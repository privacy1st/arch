# mkinitcpio.conf

After changing the global configuration,
one should execute:

```shell
mkinitcpio -P
```

## compression

Can be enabled with:

```shell
COMPRESSION='lz4'
```

## hooks

Hooks to for

1) resuming
2) BTRFS
3) encryption
4) keymap

```shell
HOOKS=(base udev modconf block keyboard keymap encrypt lvm2 btrfs filesystems fsck resume)
```

## crc32c_intel

* Archwiki (Ext4#Enabling_metadata_checksums)[https://wiki.archlinux.org/title/Ext4#Enabling_metadata_checksums]
  > If the CPU supports SSE 4.2, make sure the crc32c_intel 
  > kernel module is loaded in order to enable the hardware 
  > accelerated CRC32C algorithm
* https://en.wikipedia.org/wiki/SSE4#SSE4.2
* https://bugzilla.redhat.com/show_bug.cgi?id=1874808

On a SSE4.2 capable system, `crc32c-intel` should be used, not `crc32c-generic`.

Check your CPU (see the `.install` script):
```shell
lscpu | grep sse4_2
```

Can be enabled with:
```shell
MODULES=('crc32c-intel')
```
