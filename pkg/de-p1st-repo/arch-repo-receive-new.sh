#!/bin/bash

source /usr/lib/de-p1st-repo/util.sh    || exit $?
source /usr/lib/de-p1st-repo/pkgver.sh  || exit $?
source /usr/lib/de-p1st-repo/pkginfo.sh || exit $?
source /etc/de-p1st-repo/arch-repo.cfg  || exit $?



function main(){
  cd "${REMOTE_PKG_DIR}" || return $?
  add_new_to_db          || return $?
  generate_index         || return $?
}


#
# add all packages to database
#
function add_all_to_db(){
  echo "Adding all packages to db ..."
  sort_all_pkgname_pkgver || return $?

  echo "For each package name: Add latest version to database ..."
  for PKGNAME in db/*; do
    PKGNAME=$(basename "${PKGNAME}") || return $? # strip directory and suffix from filename
    add_to_db "${PKGNAME}" || return $?
  done
}
#
# add new packages to database
#
function add_new_to_db(){
  echo "Adding new packages to db ..."
  sort_new_pkgname_pkgver || return $?

  echo "For each new package name: Add latest version to database ..."
  for PKGNAME in "${NEW_PKGNAMES[@]}"; do
    add_to_db "${PKGNAME}" || return $?
  done
}
#
# add package to database
#
function add_to_db(){
  # $1: package name
  local PKGNAME
  PKGNAME="$1"

  # get path to latest version of $PKGNAME
  PKG=$(latest_pkgver_path "${PKGNAME}") || return $?

  # add to database
  repo-add --new "${REMOTE_DB_NAME}.db.tar.gz" "${PKG}" || return $?
}


#
# create files "db/$pkgname/$pkgver" with content "$PKG" (path to package file)
#
function sort_all_pkgname_pkgver(){
  echo "Cleanup ..."
  rm -r db || return $?

  echo "Sorting all packages by package name and package version ..."

  for PKG in *.pkg.tar.{xz,zst}; do
    sort_pkgname_pkgver "${PKG}" || return $?
  done
}
#
# create files "db/$pkgname/$pkgver" with content "$PKG" (path to package file)
#
function sort_new_pkgname_pkgver(){
  # return: array $NEW_PKGNAMES

  echo "Sorting new packages by package name and package version ..."

  local NEW_PKGNAMES_TMP=() # list the names from new package-files; may contain duplicates

  mapfile -t PKGS < <(cat new-pkg.txt)
  for PKG in "${PKGS[@]}"; do
    sort_pkgname_pkgver "${PKG}" || return $?
    NEW_PKGNAMES_TMP+=("${PKGNAME}")
  done


  # create array $NEW_PKGNAMES without duplicates
  NEW_PKGNAMES=()
  for NEW_PKGNAME_TMP in "${NEW_PKGNAMES_TMP[@]}"; do
    local contains="0"

    # if NEW_PKGNAMES does already contain NEW_PKGNAME_TMP,
    # then set contains to "1"
    for i in "${NEW_PKGNAMES[@]}"; do
      if [ "${NEW_PKGNAME_TMP}" = "${i}" ]; then
        contains="1";
        break;
      fi
    done

    if [ "${contains}" = "0" ]; then
      NEW_PKGNAMES+=("${NEW_PKGNAME_TMP}")
    fi
  done
}
#
# create files "db/$pkgname/$pkgver" with content "$PKG" (path to package file)
#
function sort_pkgname_pkgver(){
  # $1: PKG (path to package file)
  # return: variables $PKGINFO, $PKGNAME, $PKGVER
  local PKG
  PKG="$1"

  get_pkginfo "$PKG"                || { echo "get_pkginfo failed"; return 1; }
  PKGNAME=$(get_pkgname "$PKGINFO") || { echo "get_pkgname failed"; echo "Content of PKGINFO: ${PKGINFO}"; return 1; }
  PKGVER=$(get_pkgver  "$PKGINFO")  || { echo "get_pkgver  failed"; echo "Content of PKGINFO: ${PKGINFO}"; return 1; }

  echo "Creating file ./db/${PKGNAME}/${PKGVER} with content ${PKG} ..."
  mkdir -p "db/${PKGNAME}" || return $?
  echo "${PKG}" > "db/${PKGNAME}/${PKGVER}" || return $?
}



#
# generate index.html
#
function generate_index(){
  echo "Generating index.html with links to all packages ..."

  echo "<!DOCTYPE html>
<html>
<head>
<title>${HTML_TITLE}</title>
</head>
<body>
<h1>${HTML_HEADING}</h1>
<p>The sources can be found here: <a href=\"${HTML_LINK_SRC}\">${HTML_LINK_SRC}</a></p>

<ul>
" > index.html

  for PKG in *.pkg.tar.{xz,zst}; do
    echo "<li><a href=\"$PKG\">$PKG</a></li>" >> index.html;
  done

  echo '
</ul>

</body>
</html>' >> index.html
}

main "$@"
