#!/bin/bash
#
# source: https://stackoverflow.com/a/13649357
#

a=(aa ac aa ad "ac ad")
declare -A b
for i in "${a[@]}"; do b["$i"]=1; done

for i in "${!b[@]}"; do
  echo ">> $i"
done
