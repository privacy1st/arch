#
# get content of .PKGINFO from package-file
#
function get_pkginfo(){
  # $1: path to package file
  # return: variable $PKGINFO

  if endswith "$1" ".pkg.tar.xz"; then
    PKGINFO=$(tar -xf "$1" -O .PKGINFO --force-local)         || { echo "tar failed"; return 1; }
  elif endswith "$1" ".pkg.tar.zst"; then
    PKGINFO=$(tar -I zstd -xf "$1" -O .PKGINFO --force-local) || { echo "tar failed"; return 1; }
  else
    echo "$1 does not seem to be a package!"
    return 1
  fi
}


#
# get pkgname from $PKGINFO
#
function get_pkgname(){
  # return: stdout: package name

  # remove "pkgname = " as well as tailing whitespace characters
  local tmp
  tmp=$(echo "${PKGINFO}" | grep '^pkgname =')                   || { echo "grep failed"; return 1; }
  local PKGNAME
  PKGNAME=$(echo "${tmp}" | sed 's|^pkgname\s*=\s*||; s|\s*$||') || { echo "sed failed"; return 1; }

  echo "${PKGNAME}"
}
#
# get pkgver from $PKGINFO
#
function get_pkgver(){
  # return: stdout: package version

  # remove "pkgver = " as well as tailing whitespace characters
  local tmp
  tmp=$(echo "${PKGINFO}" | grep '^pkgver =')                   || { echo "grep failed"; return 1; }
  local PKGVER
  PKGVER=$(echo "${tmp}"  | sed 's|^pkgver\s*=\s*||; s|\s*$||') || { echo "sed failed"; return 1; }

  echo "${PKGVER}"
}
#
# get url from $PKGINFO
#
function get_pkgurl(){
  # return: stdout: url

  # remove "url = " as well as tailing whitespace characters
  local tmp
  tmp=$(echo "${PKGINFO}" | grep '^url =')                   || { echo "grep failed"; return 1; }
  local PKGURL
  PKGURL=$(echo "${tmp}"  | sed 's|^url\s*=\s*||; s|\s*$||') || { echo "sed failed"; return 1; }

  echo "${PKGURL}"
}
