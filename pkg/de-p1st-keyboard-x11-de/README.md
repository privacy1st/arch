# xorg / X11 keyboard configuration

==> xkb `model`, `layout` and `variant`

---

`setxkbmap -query`
* list current xkb keymap config

`localectl list-x11-keymap-layouts`
* list xkb layouts, e.g. "de" for German and "no" for Norwegian

`localectl list-x11-keymap-variants de`
* list xkb variants for the layout "de", e.g. "nodeadkeys"

---

`cat /usr/share/X11/xkb/rules/base.lst`
* list all models, layouts and variants with description

---

`/etc/X11/xorg.conf.d/00-keyboard.conf`
* persistent xorg keymap

`localectl --no-convert set-keymap de-latin1-nodeadkeys`
* generates `/etc/X11/xorg.conf.d/00-keyboard.conf`

`setxkbmap -rules xorg -layout de -variant nodeadkeys -option "grp:alt_caps_toggle"`
* temporarily change xorg keymap

---

`man xkeyboard-config`
* OPTIONS -> Switching to another layout
  * grp:alt_caps_toggle <=> Alt+Caps Lock
    
`man setxkbmap` and [https://www.x.org/releases/X11R7.5/doc/input/XKB-Config.html](https://www.x.org/releases/X11R7.5/doc/input/XKB-Config.html)
* XkbVariant - variant(s) of the layout you intend to use
* The XkbVariant option has been left out. That means the default variant named "basic" is loaded.
* equivalency between "00-keyboard.conf" config file and "setxkbmap" command

---

Nice writeup: [https://simpleit.rocks/linux/switch-keyboard-layouts/](https://simpleit.rocks/linux/switch-keyboard-layouts/)
