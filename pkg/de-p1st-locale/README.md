# locale

## locale.conf

* Archwiki: [Locale](https://wiki.archlinux.org/index.php/Locale)
  * -> run `locale -a` to list enabled locales and check for error messages such as `locale: Cannot set LC_CTYPE to default locale: No such file or directory`

* (German) Archwiki: [complete locale.conf example](https://wiki.archlinux.de/title/Locale.conf)

## locale.gen

1) Edit `/etc/locale.gen`
2) Run `locale-gen`

From Archwiki: [locale#Generating_locales](https://wiki.archlinux.org/title/locale#Generating_locales)

> locale-gen also runs with every update of glibc. [1](https://github.com/archlinux/svntogit-packages/blob/packages/glibc/trunk/glibc.install#L2)

## timezone

Archwiki: [Installation_guide#Time_zone](https://wiki.archlinux.org/index.php/Installation_guide#Time_zone)
