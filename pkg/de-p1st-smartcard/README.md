## Using a smartcard

* archwiki [Smartcards](https://wiki.archlinux.org/index.php/Smartcards)

```shell
sudo pacman -S ccid opensc
```

* If the card reader does not have a PIN pad, append the line(s) and set `enable_pinpad = false` in the opensc configuration file `/etc/opensc.conf`:

```shell
echo "enable_pinpad = false" | sudo tee -a /etc/opensc.conf >/dev/null
sudo systemctl enable --now pcscd.service
```

## Configuration

```shell
sudo pacman -S de-p1st-gnupg
```
