if ! gpg --export-ownertrust | grep --quiet '^94F3D3DDAC22802258FC044B6C47C753F0823002:6:$'; then
  # Import public key.

  # Write to stderr.
  echo "Importing public key ..."  >&2

  gpg --import /usr/share/gnupg/94F3D3DDAC22802258FC044B6C47C753F0823002.pub   || {
    echo "p1st: Error importing pubkey!";
    return 1;
  }
  echo "94F3D3DDAC22802258FC044B6C47C753F0823002:6:" | gpg --import-ownertrust || {
    echo "p1st: Error changing trust!"
    return 1;
  }
fi
