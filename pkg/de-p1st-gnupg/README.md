# gnupg

**TODO**:
* Currently using graphical pinentry ... this would mean dependend on X11/wayland
  * could this be done with holo?
  * default to terminal-pinentry
  * `de-p1st-gnupg-x11` then changes the /etc/skel files to use graphical-pinentry

---

GnuPG german mini HowTo:
* [pdf](GnuPG_MiniHowto_ger_20200215.pdf)

`makepkg` and `pacman` use different PGP keyrings:
* [Two PGP Keyrings in Arch Linux](Two%20PGP%20Keyrings%20for%20Package%20Management%20in%20Arch%20Linux%20%7C%20Allan%20McRae.pdf)

One can use `/etc/gnupg/gpgconf.conf` to configure gpg and gpg-agent. However, not all options are available ...

```shell
gpgconf --list-options gpg
gpgconf --list-options gpg-agent
```

Using a smartcard:
* kuketz-blog.de: [gnupg-public-key-authentifizierung-nitrokey-teil3](https://www.kuketz-blog.de/gnupg-public-key-authentifizierung-nitrokey-teil3/)
* [gnupg.org -> Invoking-GPG_AGENT](https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html)

## gpg.conf

Location: `~/.gnupg/gpg.conf`

* [https://riseup.net/en/security/message-security/openpgp/best-practices]()
  * [https://github.com/ioerror/duraconf/blob/master/configs/gnupg/gpg.conf]()
  
## gpg-agent.conf

Location: `~/.gnupg/gpg-agent.conf`

```
# List pinentries: pacman -Ql pinentry | grep /usr/bin/
# If a graphical application shall use ones smartcard one needs to specify a graphical pinentry program.
pinentry-program /usr/bin/pinentry-gnome3

# Enable ssh to use a smartcard for authentication.
enable-ssh-support
```

Debug options:

```
debug-pinentry
debug ipc
verbose
log-file /home/__USER__/.gnupg/logfile.log
```

`gnupg` depends on `pinentry` and `pinentry-gnome3` is part of `pinentry`.

```
$ pacman -F /usr/bin/pinentry-gnome3
usr/bin/pinentry-gnome3 is owned by core/pinentry 1.1.1-1
```

## Graphical Login: /etc/profile.d/*.sh, bashrc, .zshrc.local

* Archwiki: [GnuPG#Configure_pinentry_to_use_the_correct_TTY](https://wiki.archlinux.org/index.php/GnuPG#Configure_pinentry_to_use_the_correct_TTY)
* These two shell lines are demanded by the gnupg documentation in the chapter `Invoking GPG-AGENT`
* man 1 gpg-agent -> EXAMPLES -> set env variable GPG_TTY in your login shell

One's interactive, non-login shell, should run this:

```shell
GPG_TTY=$(tty)
export GPG_TTY
gpg-connect-agent updatestartuptty /bye >/dev/null
```

## SSH_AUTH_SOCK: /etc/profile.d/*.sh, bashrc, .zshrc.local

* Archwiki: [GnuPG#Set_SSH_AUTH_SOCK](https://wiki.archlinux.org/index.php/GnuPG#Set_SSH_AUTH_SOCK)

One's interactive, non-login shell, should run this:

```shell
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
  export SSH_AUTH_SOCK
fi
```

## Note about "interactive, non-login, shell"

The gnupg manual is talking about "login shell" but mentions "~/.bashrc",
so I assume they mean an "interactive, non-login, shell".
See https://wiki.archlinux.org/title/bash#Configuration_files

Correct files to set `SSH_AGENT_PID` and `GPG_TTY`:

* `/etc/bash.bashrc`
* `/etc/zsh/zshrc`

These do not work:

* `/etc/profile.d/99_gnupg.sh`
  > `/etc/profile` This file should be sourced by all POSIX sh-compatible shells
  > upon login: it sets up $PATH and other environment variables and application-specific
  > (/etc/profile.d/*.sh) settings upon login.
* `/etc/X11/xinit/xinitrc.d/`


## Use smartcard on new computer

To be able to use a smartcard, one has to import and then trust the public key first!

```shell
gpg --import 94F3D3DDAC22802258FC044B6C47C753F0823002.pub
```

And then trust the key:

```shell
gpg --edit-key 0x94F3D3DDAC22802258FC044B6C47C753F0823002
trust
5
y
quit
```

or

```shell
printf "5\ny\nquit\n" | gpg --command-fd 0 --expert --edit-key 0x94F3D3DDAC22802258FC044B6C47C753F0823002 trust
```

or

```shell
echo "94F3D3DDAC22802258FC044B6C47C753F0823002:6:" | gpg --import-ownertrust
```

See also:
* [export/import ownertrust](https://superuser.com/a/1125128)

This could be done with a script [99_import_pubkey.sh](99_import_pubkey.sh)
placed inside `/etc/profile.d/`. When using `sddm` as login manager, then
the output of this script can be found in `~/.local/share/sddm/xorg-session.log`.
