# redsihft

```shell
sudo pacman -S --needed redshift
```

* Session and Startup -> add entry
  -> on Login execute `redshift -l 49.5:10.9`
  (for Erlangen, Germany)

## config

[redshift repo](https://github.com/jonls/redshift)

[redshift sample config](https://raw.githubusercontent.com/jonls/redshift/master/redshift.conf.sample)

Config file: Linux/macOS: `~/.config/redshift/redshift.conf`
(if the environment variable `XDG_CONFIG_HOME` is undefined)
or `${XDG_CONFIG_HOME}/redshift/redshift.conf` (if `XDG_CONFIG_HOME`
is defined).

```shell
mkdir -p ~/.config/redshift

cat << 'EOF' > ~/.config/redshift/redshift.conf
[redshift]
temp-day=5000
temp-night=3800

; Disable the smooth fade between temperatures when Redshift starts and stops.
; 0 will cause an immediate change between screen temperatures.
; 1 will gradually apply the new screen temperature over a couple of seconds.
fade=1

; Custom dawn/dusk intervals.
; Instead of using the solar elevation, the time intervals of dawn and dusk
; can be specified manually. The times must be specified as HH:MM in 24-hour
; format.
dawn-time=11:00-12:00
dusk-time=15:00-16:00
gamma=0.8

location-provider=manual
adjustment-method=randr
[manual]
lat=49.5
lon=10.9
EOF
```
