# My personalized Arch Linux distribution

## Cloning this repository

```shell
git clone --recurse-submodules https://codeberg.org/privacy1st/arch && cd arch
```

Or equivalently:

```shell
git clone https://codeberg.org/privacy1st/arch
cd arch
git submodule update --init --recursive
```

## Build packages from source

Fork this repository.

Then and adjust the following files:
* [pkg/de-p1st-repo/arch-repo.cfg](pkg/de-p1st-repo/arch-repo.cfg)
  * For your build-machine, adjust section `LOCAL MACHINE CONFIGURATION`: Add absolute path of folder [build-pkg/out](build-pkg/out) to array `LOCAL_PKG_DIRS`
    as the build packages will be stored there.
  * For your mirror-server, adjust section `REMOTE MIRROR SERVER CONFIGURATION` accordingly.
* [pkg/de-p1st-pacman/pacman.d/de-p1st](pkg/de-p1st-pacman/pacman.d/de-p1st)
  * Add the address of your mirror-server.

Build [de-p1st-repo](pkg/de-p1st-repo):

```shell
cd build-pkg && sudo docker-compose run --rm makepkg de-p1st-repo
```

Install it on your build-machine and your mirror-server:

```shell
# On your local machine
sudo pacman -U out/de-p1st-repo*.pkg.tar.*

# Copy the package to your mirror-server and install it there as well!
```

Then you can start building all packages and adding them to your mirror-server:

```shell
cd build-pkg && ./build-all
```

## Adding/removing AUR packages

There are several [AUR](https://aur.archlinux.org/) packages added as submodules inside [pkg](pkg).
See [build-pkg/pkglist-AUR.txt](build-pkg/pkglist-AUR.txt) for a full list.

They were added in the following manner:

```shell
PKG=xml2
git submodule add "https://aur.archlinux.org/${PKG}.git" pkg/"${PKG}"
# echo "Add ${PKG} to 'build-pkg/pkglist-AUR.txt'. Each line represents one build stage."
```

And can be removed with:

```shell
PKG=xml2
git rm -f pkg/"${PKG}"  # this should remove from .gitmodules

rm build-pkg/out/"${PKG}"<version>
```

If a package has been pushed to a remote repository, read [pkg/de-p1st-repo/README.md](pkg/de-p1st-repo/README.md)
for details on how to remove it from there.

## Update submodules of AUR packages

To update all submodules and add those who received an update to
[build-pkg/pkglist.tmp](build-pkg/pkglist.tmp), run

```shell
./git-pull
```

## Build outdated AUR packages

The following command includes `./git-pull`.

```shell
cd build-pkg && ./pull-and-build-outdated
```


## Install Arch Linux

See [pkg/de-p1st-installer/README.md](pkg/de-p1st-installer/README.md)


## Notes and TODOs

See [notes.md](notes.md)
