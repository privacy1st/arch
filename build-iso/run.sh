#!/bin/bash

BUILD_DIR=/out

PKGS=()
PKGS+=('de-p1st-keyboard')  # german keyboard
PKGS+=('de-p1st-nano')      # syntax hilighting in nano
PKGS+=('de-p1st-screen')    # longer scrollback history in screen
PKGS+=('de-p1st-pacman')    # [de-p1st] mirror enabled
PKGS+=('de-p1st-installer') # de-p1st-installer script

# Uncomment/remove these two lines to disable password-less ssh
ENABLE_SSH=true
SSH_PUB_KEY='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCpgihAg8Qzu5q+AGXHLR7p+rrS1yB0KlZb/Y/EwZT15EhEtsUMqBMRiY0TdjKQU0broyygZnymccLmCXvihEgk3zk/hP8VEDmN5wmE2tRBPR4lSjo8E1R8N79G+gaFkwi93QYh57MsGfa9k4tvGrJy0yaD7GrPqtQf+IIuvV14WJQAqnikTdbFqRjk5JGearYLU7jSKa+9NmR7JQ9NExoyIPgmQ/pd0Xc2qt8k5UGfz3HM9MAmIVQ30whK6m1iYZ8nxEidHrfreQx8NOa7ujo4zQnV1NYvRUjObr/qyIhPU6DYLT2mVRNupQFKx6LI38O4U13heugUFqJ3zvog3aDsriFiv8jzJAJvWXx7Q3TqKhqiG9VTkwBw1NDbCAOXKiEdMfiCYbdCfpNgdepU75bMloJcSQQVUqoH2tQhSbwKLuRFgOnyaHpvWbieXBRcUnfG8gg4p4jqiwx5laweEeOIOD/i7G1Mjx7Dj2ctnT/ILat/xsf+Y0W4eJr3bc5L9ghgw6wsKbNSqwjFUCFcHcARK3gvSH+hO2/BpgMVoyvZjO5PNuUqfsZ7bIIs5cDdyB/ly3irKuaRz1+3x1x4gPgSiOcji7HxPwogzhPsyfoRLHNt9tJ5X4nF2Iz1M5RTJpbZCi6yEj+9Q85FVjD76BEWuZe18kRrwhuLf/XgKdF9tQ== openpgp:0xA8B75370'

ISO_PUBLISHER='Daniel Langbein <https://codeberg.org/privacy1st/arch>'
ISO_LABEL='ARCH-P1ST_$(date +%Y%m%d)'
ISO_NAME="arch-p1st"

################################

# Write-permission for user "build"
sudo chown "build:wheel" "${BUILD_DIR}"

function isEmptyDir() {
  # arg $1: directory to check

  if [ -z "$(find "${1}" -maxdepth 0 -type d -empty 2>/dev/null)" ]; then
    # echo "Not empty or NOT a directory"
    return 1
  else
    # "Empty directory"
    return 0
  fi
}

if [ -d "${BUILD_DIR}" ]; then
  if ! isEmptyDir "${BUILD_DIR}"; then
    if [ "$1" = "clean" ]; then
      sudo rm -r "${BUILD_DIR}"/* || exit $?
    else
      echo "BUILD_DIR not empty: ${BUILD_DIR}";
      echo "Run with argument 'clean' to clean up BUILD_DIR and then continue building the ISO."
      exit 1;
    fi
  fi # else: BUILD_DIR exists and is empty :)
elif ! mkdir -p "${BUILD_DIR}"; then
  echo "Could not create BUILD_DIR: ${BUILD_DIR}";
  exit 1;
fi

# Create work and out directories
mkdir "${BUILD_DIR}"/work_dir && mkdir "${BUILD_DIR}"/out_dir || exit $?
# Then copy a profile as starting point.
# The releng profile is used to create the official monthly installation ISO
PROFILE=/usr/share/archiso/configs/releng/
if [ ! -d "${PROFILE}" ]; then
  echo "Dependency 'archiso' is missing. Please install this first!"
  exit 1
fi
cp -r "$PROFILE" "$BUILD_DIR"/profile || exit $?


# Extend the builder's pacman.conf (add de-p1st mirrors)
# so that packages from [de-p1st] can be installed into the ISO
# https://wiki.archlinux.org/index.php/Archiso#Custom_local_repository
#
# cat ../pkg/de-p1st-pacman/pacman.d/de-p1st >>"$BUILD_DIR"/profile/pacman.conf || exit $?
echo '[de-p1st]
SigLevel = Optional TrustAll
Server = https://arch.p1st.de' >> "$BUILD_DIR"/profile/pacman.conf || exit $?

for PKG in "${PKGS[@]}"; do
  echo "${PKG}" >>"${BUILD_DIR}"/profile/packages.x86_64 || exit $?
done


if [ "${ENABLE_SSH}" = "true" ]; then
  MOTD_SSH_NOTE="
The installation can be done via ssh. See [35m.ssh/authorized_keys[0m.
"
else
  MOTD_SSH_NOTE=""
fi

# source: https://gitlab.archlinux.org/archlinux/archiso/-/blob/master/configs/releng/airootfs/etc/motd
echo "To install [38;2;23;147;209mArch Linux[0m run the following command:
[35mde-p1st-installer[0m
${MOTD_SSH_NOTE}
For Wi-Fi, authenticate to the wireless network using the [35miwctl[0m utility.
For mobile broadband (WWAN) modems, connect with the [35mmmcli[0m utility.
Ethernet, WLAN and WWAN interfaces using DHCP should work automatically.
" > "${BUILD_DIR}"/profile/airootfs/etc/motd || exit $?


if [ "${ENABLE_SSH}" = "true" ]; then
  # Enabling sshd.service is not necessary for the releng profile: It is enabled by default.
  # (Although one has to first set a password before he can connect via ssh.
  # See: https://wiki.archlinux.org/title/Install_Arch_Linux_via_SSH
  #
  # We will prepare the ISO for passwordless installation via ssh.
  # See: https://wiki.archlinux.org/title/Archiso#Prepare_an_ISO_for_an_installation_via_SSH
  # And we will (optionally) use a custom sshd_config file

  # Overwrite the ssh configuration file of the releng profile
  # It is **important** to allow root login here!
  echo 'PermitRootLogin yes
PubkeyAuthentication yes
AuthorizedKeysFile	.ssh/authorized_keys
PasswordAuthentication no
PermitEmptyPasswords no
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding no
PrintMotd no # pam does that
Subsystem	sftp	/usr/lib/ssh/sftp-server' > "${BUILD_DIR}"/profile/airootfs/etc/ssh/sshd_config || exit $?

  # Create authorized_keys file for root so that one can ssh into that user
  mkdir -p                "${BUILD_DIR}"/profile/airootfs/root/.ssh/ || exit $?
  echo "${SSH_PUB_KEY}" > "${BUILD_DIR}"/profile/airootfs/root/.ssh/authorized_keys || exit $?
  # Set correct file permissions.
  echo 'file_permissions+=(
  ["/root/.ssh"]="0:0:0700"
  ["/root/.ssh/authorized_keys"]="0:0:0600"
)' >> "${BUILD_DIR}"/profile/profiledef.sh || exit $?

  # Manually create ssh host key files.
  # Usually this is done during first boot of the ISO,
  # but we want to know the key fingerprints!
  # See also: https://serverfault.com/a/471346
  ssh-keygen -N "" -t dsa -f         "${BUILD_DIR}"/profile/airootfs/etc/ssh/ssh_host_dsa_key     || exit $?
  ssh-keygen -N "" -t rsa -b 4096 -f "${BUILD_DIR}"/profile/airootfs/etc/ssh/ssh_host_rsa_key     || exit $?
  ssh-keygen -N "" -t ecdsa -f       "${BUILD_DIR}"/profile/airootfs/etc/ssh/ssh_host_ecdsa_key   || exit $?
  ssh-keygen -N "" -t ed25519 -f     "${BUILD_DIR}"/profile/airootfs/etc/ssh/ssh_host_ed25519_key || exit $?
  # Set correct file permissions.
  echo 'file_permissions+=(
  ["/etc/ssh/ssh_host_dsa_key"]="0:0:0600"
  ["/etc/ssh/ssh_host_dsa_key.pub"]="0:0:0644"
  ["/etc/ssh/ssh_host_rsa_key"]="0:0:0600"
  ["/etc/ssh/ssh_host_rsa_key.pub"]="0:0:0644"
  ["/etc/ssh/ssh_host_ecdsa_key"]="0:0:0600"
  ["/etc/ssh/ssh_host_ecdsa_key.pub"]="0:0:0644"
  ["/etc/ssh/ssh_host_ed25519_key"]="0:0:0600"
  ["/etc/ssh/ssh_host_ed25519_key.pub"]="0:0:0644"
)' >> "${BUILD_DIR}"/profile/profiledef.sh || exit $?

  # Save ssh host fingerprints next to the ISO file
  for f in "${BUILD_DIR}"/profile/airootfs/etc/ssh/ssh_host_*_key; do
    ssh-keygen -l -f "$f" >> "${BUILD_DIR}"/out_dir/ssh-host-fingerprints || exit $?
  done

  # Enable sshd.service
  # https://wiki.archlinux.org/title/Archiso#systemd_units
  # mkdir -p "${BUILD_DIR}"/profile/airootfs/etc/systemd/system/multi-user.target.wants/ || exit $?
  # ln -s /usr/lib/systemd/system/sshd.service \
  #   "${BUILD_DIR}"/profile/airootfs/etc/systemd/system/multi-user.target.wants/ || exit $?
fi


# Replace the "iso_publisher", "iso_label" and "iso_name"
sed -i "s|^iso_publisher=.*\$|iso_publisher=\"${ISO_PUBLISHER}\"|" "${BUILD_DIR}"/profile/profiledef.sh || exit $?
sed -i "s|^iso_label=.*\$|iso_label=\"${ISO_LABEL}\"|"             "${BUILD_DIR}"/profile/profiledef.sh || exit $?
sed -i "s|^iso_name=.*\$|iso_name=\"${ISO_NAME}\"|"                "${BUILD_DIR}"/profile/profiledef.sh || exit $?

################################

echo "running 'sudo mkarchiso' ..."
sudo mkarchiso -v -w "${BUILD_DIR}"/work_dir -o "${BUILD_DIR}"/out_dir "${BUILD_DIR}"/profile || exit $?

sudo chown -R "build:wheel" "${BUILD_DIR}"/out_dir || exit $?
echo "DONE. See ./out/out_dir for the generated .iso file and it's ssh fingerprints."
