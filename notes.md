# Notes and TODOs

## Some notes on arch (meta-)package management

* [https://disconnected.systems/blog/archlinux-meta-packages/]()
* [https://nerdstuff.org/posts/2020/2020-002_meta_packages/]()

* [https://gitlab.com/archi3linux/meta/-/blob/master/PKGBUILD]()
* [https://github.com/alfredopalhares/arch-pkgbuilds]()

### Managing custom packages; managing a remote repository

1) One could use [crema](https://gitlab.com/mipimipi/crema)
2) Or some self-made shell scripts: [pkg/de-p1st-repo/README.md](pkg/de-p1st-repo/README.md)


## Notes about PKGBUILD

### Package naming

* [package naming](https://wiki.archlinux.org/index.php/Arch_package_guidelines#Package_naming)

Package names are prefixed with `de-p1st` as this is one of my
domains ([p1st.de](https://p1st.de)) which itself is an abbreviation of
[privacy1st.de](https://privacy1st.de)

### Creating config files

```shell
install -Dm644 $src $pkgdir/$dst
```

Example: [pkg/de-p1st-sudo/PKGBUILD](pkg/de-p1st-sudo/PKGBUILD)

### Changing existing config files

* `holo` from the AUR
* [https://github.com/holocm/holo/blob/master/doc/holo-files.8.pod]()

```shell
install -Dm0544 some-config.conf.holoscript "$pkgdir"/usr/share/holo/files/20-"$pkgname"/etc/path/to/some-config.conf.holoscript
```

Example:
* [pkg/de-p1st-pacman/pacman.conf.holoscript](pkg/de-p1st-pacman/pacman.conf.holoscript)
* [pkg/de-p1st-pacman/PKGBUILD](pkg/de-p1st-pacman/PKGBUILD)

### Changing config files that are not owned by any package

See [pkg/de-p1st-locale/PKGBUILD](pkg/de-p1st-locale/PKGBUILD) for an example.

### Home skeleton

Pacman should **never** change files in `$HOME`. To still be able to include
customized configurations, one can copy them to the skeleton used for new users:

Files from `/etc/skel` are copied to `/home/$USER` when new users are created.

Example: [pkg/de-p1st-gnupg/PKGBUILD](pkg/de-p1st-gnupg/PKGBUILD)

### Enabling services

* systemd.preset - Service enablement presets
* [man 5 systemd.preset](https://www.systutorials.com/docs/linux/man/5-systemd.preset/)

**Note**: the preset name shall start with a two-digit number < 99.

```shell
install -Dm0644 systemd.preset "$pkgdir"/usr/lib/systemd/system-preset/20-"$pkgname".preset
```

Example package:
* [pkg/de-p1st-networkmanager/systemd.preset](pkg/de-p1st-networkmanager/systemd.preset)
* [pkg/de-p1st-networkmanager/PKGBUILD](pkg/de-p1st-networkmanager/PKGBUILD)

**Note**:

Running `systemctl preset-all` resets all installed unit files to the defaults configured in the preset policy files.

This implies: **All manual changes** such as `systemctl enable serviceXYZ` will get lost!

To avoid this, enable your services with systemd-presets!

```shell
echo 'enable NetworkManager.service' | sudo tee -a /usr/lib/systemd/system-preset/20-custom.preset
sudo systemctl preset-all
```

### Multiple providers

Example:
* Two packages (`de-p1st-test2` and `de-p1st-test3`) provide `de-p1st-test`
* If one installs `de-p1st-test` he can interactively choose one which provider to select:

```
$ sudo pacman -S de-p1st-test
:: There are 2 providers available for de-p1st-test:
:: Repository de-p1st
   1) de-p1st-test2  2) de-p1st-test3

Enter a number (default=1): 
```

## TODOs (Ordered by priority)

* generate PKGBUILD files with python

* split up de-p1st-mkinitcpio
  * one "base" mkinitcpio
  * one additional for "encrypt" hook
  * another additional for "resume" hook

* Install more packages with a Makefile, see [toggle-bluetooth/Makefile](pkg/toggle-bluetooth/Makefile)
* de-p1st-cups

* remove "de-p1st-grub" from base
  * just one "base" package for both: BIOS and (U)EFI installation!
* installer: support BIOS boot mode? Or drop BIOS support?
* installer: systemd-boot as alternative to GRUB?
  * https://wiki.archlinux.org/title/systemd-boot

* for each PKG: built with docker then sign wit pgp
  * signed package db

* How does Pacman pick the default option? Are packages simply
  ordered alphabetically?

* build for multiple architectures
  * https://ownyourbits.com/2018/06/27/running-and-building-arm-docker-containers-in-x86/
  * https://ownyourbits.com/2018/06/27/running-and-building-arm-docker-containers-in-x86/#comment-19124
